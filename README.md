#Films en React JS

Lancement du projet par "npm start" dans le dossier films-reactjs.

A l'attention de Mr Grandin & Mr Devigne:

Bonjour,

Comme demandé, je vous donne l'accès à mon projet github pour l'exercice de ReactJS.
Je vous le transmet aujourd'hui puisque vous aviez demandé à ce qu'il soit rendu pour cette semaine,
mais comme vous le verrez, je n'ai pas pu le terminer complètement.
Ayant un travail saisonnier à plein temps, j'ai eu du mal à concilier cette demande avec mes autres obligations et je m'en excuse.
Je compte terminer le projet ce Week-end en améliorant le visuel de l'affichage des films et en ajoutant la recherche de films.

Cordialement

Ducornet Theo