const APIkey= 'd4dfced817985d414b727774821c9678';

/*
    Connect to TheMovieDataBase api, fetch and return the 1st page of popular movies.
 */
export async function getPopularMovies() {
    let data = []
    try {
        const response = await fetch('https://api.themoviedb.org/3/movie/popular?api_key='+APIkey+'&language=fr-FR&page=1')
        const responseData = await response.json()
        data = responseData?.results

    } catch (error) {console.log(error)}

    return data
}