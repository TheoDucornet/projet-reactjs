import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';

const MovieController = require('./movieController');

/*
    Component used to display more information on a single movie.
    This component is the main parent of other components.
 */
class SelectedMovie extends React.Component{

    state = {
        movieToDisplay: null
    }

    // Used to refresh this component when a movie in one the children component is clicked
    handleMovieToDisplayChange = movieToDisplay =>{
        this.setState({movieToDisplay})
    }

    render(){
        // Display only list of popular movies on 1st load
        if(!this.state.movieToDisplay){
            return (
                <div>
                    <MovieList movieToDisplay={this.state.movieToDisplay} onMovieToDisplayChange={this.handleMovieToDisplayChange}/>
                </div>
            )
        }

        console.log(this.state.movieToDisplay);
        //It's easier to set div style here because it depends on the selectedMovie variable
        const selectedMovieMainDivBackground = {
            background: " center / cover no-repeat url(https://image.tmdb.org/t/p/w1920_and_h800_multi_faces/"+this.state.movieToDisplay.backdrop_path+")"
        };

        return (
            <div>
                <div style={selectedMovieMainDivBackground} id="SelectedMovieMain">
                    <div className="coverBackgroundImage">
                        <img className="selectedMovieListImage" src={'http://image.tmdb.org/t/p/w342/'+ this.state.movieToDisplay.poster_path} alt=""/>
                        <div className="SelectedMovieText">
                            <h1>{this.state.movieToDisplay.original_title}</h1>
                            <div> <li>{"("+this.state.movieToDisplay.release_date+")"}</li></div>
                            <br></br>
                            <div className="Vote">
                                <div>{this.state.movieToDisplay.vote_average+" "}</div>
                                <div> Note des utilisateurs</div>
                            </div>
                            <h2 style={{textAlign: "left"}}>Synopsys</h2>
                            <div >{this.state.movieToDisplay.overview}</div>
                            <br></br>
                        </div>
                    </div>
                </div>
                <MovieList movieToDisplay={this.state.movieToDisplay} onMovieToDisplayChange={this.handleMovieToDisplayChange}/>
            </div>
        )
    }
}

/*
    Component used to display a list of 5 most popular movies
 */
class MovieList extends React.Component {

    constructor(props){
        super(props)
        this.state={movies : []}
    }

    render() {
        // Get 5 most popular movies
        MovieController.getPopularMovies().then(movies=> {
                let popularMovies = []
                for (let i = 0; i < 5; i++) {
                    popularMovies.push(movies[i]);
                }
                this.setState({movies:popularMovies})
        })

        // Display each movie with a title and an image. Listen if movie get clicked on.
        return (
            <div id="Recommendations">
                <h2>Films Populaires</h2>
                <div id="ListMovie">
                {this.state.movies.map((movie)=>(
                    <div className="movie" onClick={() => {
                        // Allow to refresh parent component SelectedMovie
                        this.props.onMovieToDisplayChange(movie)
                    }}>
                        <img className="movieListImage" src={'http://image.tmdb.org/t/p/w154/'+ movie.poster_path} alt=""/>
                        <div className="movieListTitle">{movie.title}</div>
                    </div>
                ))}
                </div>
            </div>
        );
    }
}

// Render everything in the body of index.html
const moviesDisplay = ReactDOM.createRoot(document.getElementById("displayMovies"));
moviesDisplay.render(<SelectedMovie/>)